import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Service} from './fmss/shared/service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxSoapModule} from 'ngx-soap';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './fmss/login/login.component';
import {DashboardComponent} from './fmss/dashboard/dashboard.component';
import {RequestInterceptor} from './fmss/shared/request-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSoapModule,
    FormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true
  },
    Service],
  bootstrap: [AppComponent]
})
export class AppModule {
}
