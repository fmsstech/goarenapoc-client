import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';
import {Service} from '../shared/service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  images: any;
  state: any;

  constructor(private service: Service, private router: Router, private route: ActivatedRoute, private sanitizer: DomSanitizer) {
  }

  postList = new Array();

  ngOnInit() {
    this.state = this.route.snapshot.params.state;
    this.getAllByState();
  }


  getAllByState() {
    let self = this;
    const state = this.state;
    const body = {state};

    this.service.post('/post-service/post/find-all-by-state', JSON.stringify(body)).subscribe(response => {
      if (!response.data) {
        Swal.fire('', 'Cannot get data!!', 'error');
      } else {
        self.postList = response.data.groupList;

        self.postList.forEach(f => {
          if (f.post.imageStr) {
            f.image = 'data:image/jpeg;base64,' + f.post.imageStr;
          }

        });
      }

    }, err => {
      Swal.fire('', 'Cannot get data!', 'error');
    });
  }

  changeState(postId, state) {
    const body = {postId, state};

    this.service.post('/post-service/post/change-state', JSON.stringify(body)).subscribe(response => {
      if (!response.data) {
        Swal.fire('', 'Cannot update!!', 'error');
      } else {
        this.getAllByState();
      }
    });
  }


  navigate(c) {
    this.state = c;
    this.getAllByState();
  }

}
