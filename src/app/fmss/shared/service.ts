import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {map, tap} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class Service {
  const
  headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

  constructor(private http: HttpClient) {
  }

  get<T>(url: string, id?: string, params?: HttpParams): Observable<any> {
    return this.http.get<T>(url, {params}).pipe(map((response: any) => {
      return response;
    }));
  }


  post<T>(url: string, body: string): Observable<any> {

    return this.http.post<T>(url, body, {headers: this.headers}).pipe(map((response: any) => {
      return response;
    }));
  }

  put<T>(url: string, body: string): Observable<any> {
    return this.http.put<T>(url, body).pipe(map((response: any) => {
      return response;
    }));
  }

  delete<T>(url: string, id: string): Observable<any> {
    return this.http.delete<T>(+url + id).pipe(map((response: any) => {
      return response;
    }));
  }

}
